## 1. Prerequisites

Execute the next command, to globally install npm packages
```
npm install -g bower cordova ionic gulp tsd
```

## 2. Installation

After checking out the project, execute the following commands in project folder:
```
bower install
```
should install bower web libraries in www/lib
	 
```	 
ionic state restore
```
command looks at the cordovaPlugins and cordovaPlatforms attributes in your package.json file
to restore your application with those platforms and plugins.
 
```
tsd reinstall -s
```
install the modules listed in tsd.json configuration file and creates the tsd.d.ts


## 3. Ionic Platforms

### 3.1 Android

To add android platform to project, execute in command line:
```
ionic platform add android
```

To remove a platform, execute
```
ionic platform rm android
```

Once the platform is added to the project, it can be build for that platform
```
ionic build android
```

To run your project in android emulator,
```
ionic emulate android
```

To see application changes live in the emulator, the following command must be executed:
```
ionic emulate android --livereload
```

Also, for having console logs or server logs displayed in terminal
```
ionic emulate android --livereload --consolelogs --serverlogs
```

Running application in emulator, suppose that you already have installed android sdk and configured an AVD for the emulator. For installing android emulator, I follow the first steps in this tutorial

http://learn.ionicframework.com/videos/windows-android

**Notes:**

1. The following environment variables has to be set:
	* ANDROID_HOME - path to the 'android-sdk' folder (e.g. d:\android-sdk)
	* ANDROID_SDK_HOME - same as above (I think different scripts use one or the other)
	* ANT_HOME - path to the ant folder (e.g. c:\java\apache-ant-1.9.2)
	* JAVA_HOME - path to the Java SDK installation folder (e.g. c:\java\jdk1.8.0_31)

2. I had problems building the application and running android sdk tools because I had installed a 32-bit JDK on a 64-bits Windows machine. So, it is recommended for a 64-bits machine, to have a 64-bit JDK installed and set as JAVA_HOME.


### 3.1 iOS

Not tested, yet. Ionic buld and emulate commands should be the same as before, just replace android with ios. The differences must be discovered.