/// <reference path='../../typings/tsd.d.ts' />

interface Window {
    cordova: any;
    StatusBar: any;
}
interface Navigator {
    language: any;
    userLanguage: any;
}

interface IIonicConfigProvider {
    tabs: any;
}

(():void => {
    'use strict';
    angular.module('app', [
        'ionic',
        'ui.router',
        'pascalprecht.translate',
        'app.home',
        'app.dash',
        'app.account',
        'app.chat',
        'app.booking',
        'app.information',
        'app.settings',
    ])
        .config(config)
        .run(main)

    angular.module('app.home', []);
    angular.module('app.dash', []);
    angular.module('app.account', []);
    angular.module('app.chat', []);
    angular.module('app.booking', []);
    angular.module('app.information', []);
    angular.module('app.settings', []);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$translateProvider', '$ionicConfigProvider'];
    function config(stateProvider:angular.ui.IStateProvider,
                    urlProvider:angular.ui.IUrlRouterProvider,
                    translateProvider:angular.translate.ITranslateProvider,
                    ionicConfigProvider:IIonicConfigProvider):void {

        configStates(stateProvider);

        // if none of the above states are matched, use this as the fallback
        urlProvider.otherwise('/tab/home');

        // initialize I18N
        translateProvider.useStaticFilesLoader({
            prefix: '/i18n/',
            suffix: '.json'
        });

        translateProvider.preferredLanguage("en");
        translateProvider.fallbackLanguage("en");

        ionicConfigProvider.tabs.position('bottom');
    }

    function configStates(stateProvider:angular.ui.IStateProvider):void {

        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js

        // setup an abstract state for the tabs directive
        var tabState:angular.ui.IState = <angular.ui.IState> {
            url: "/tab",
            abstract: true,
            templateUrl: "app/tabs.html"
        };
        stateProvider.state('tab', tabState);


        // Each tab has its own nav history stack:

        var homeState:angular.ui.IState = <angular.ui.IState> {
            name: 'tab.home',
            url: '/home',
            views: {
                'tab-home': {
                    templateUrl: 'app/components/home/home.html',
                }
            }
        };
        stateProvider.state(homeState);

        var newBookingState:angular.ui.IState = <angular.ui.IState> {
            name: 'tab.newBooking',
            url: '/newBooking',
            views: {
                'tab-newBooking': {
                    templateUrl: 'app/components/booking/newBooking.html',
                }
            }
        };
        stateProvider.state(newBookingState);

        var myBookingsState:angular.ui.IState = <angular.ui.IState> {
            name: 'tab.myBookings',
            url: '/myBookings',
            views: {
                'tab-myBookings': {
                    templateUrl: 'app/components/booking/myBookings.html',
                }
            }
        };
        stateProvider.state(myBookingsState);

        var informationState:angular.ui.IState = <angular.ui.IState> {
            name: 'tab.information',
            url: '/information',
            views: {
                'tab-information': {
                    templateUrl: 'app/components/information/information.html'
                }
            }
        };
        stateProvider.state(informationState);

        //var detailState:angular.ui.IState = <angular.ui.IState> {
        //    name: 'tab.chat-detail',
        //    url: '/chats/:chatId',
        //    views: {
        //        'tab-chats': {
        //            templateUrl: 'app/components/chats/chat-detail.html',
        //        }
        //    }
        //};
        //stateProvider.state(detailState);

        var settingsState:angular.ui.IState = <angular.ui.IState> {
            name: 'tab.settings',
            url: '/settings',
            views: {
                'tab-settings': {
                    templateUrl: 'app/components/settings/settings.html',
                }
            }
        };
        stateProvider.state(settingsState);
    }

    main.$inject = ['$ionicPlatform', '$translate'];
    function main(ionicPlatform:any,
                  translateService:angular.translate.ITranslateService):void {

        ionicPlatform.ready(():void => {
            ready(ionicPlatform, translateService);
        });
    }

    function ready(ionicPlatform:any,
                   translateService:angular.translate.ITranslateService):void {

        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            window.cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            window.StatusBar.styleLightContent();
        }

        var userLang = navigator.language || navigator.userLanguage;
        translateService.use(userLang.substr(0, 2));
    }
})();
