/// <reference path='../../../../typings/tsd.d.ts' />
var app;
(function (app) {
    var account;
    (function (account) {
        'use strict';
        var AccountController = (function () {
            function AccountController() {
                this.enableFriends = false;
            }
            return AccountController;
        })();
        angular
            .module('app.account')
            .controller('app.account.AccountController', AccountController);
    })(account = app.account || (app.account = {}));
})(app || (app = {}));
//# sourceMappingURL=account.js.map