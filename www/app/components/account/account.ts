/// <reference path='../../../../typings/tsd.d.ts' />

module app.account {
    'use strict';

    interface IAccountControllerScope {
        enableFriends: boolean;
    }

    class AccountController implements IAccountControllerScope {
        enableFriends:boolean;

        constructor() {
            this.enableFriends = false;
        }
    }

    angular
        .module('app.account')
        .controller('app.account.AccountController', AccountController);
}
