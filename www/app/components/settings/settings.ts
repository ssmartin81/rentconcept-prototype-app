/**
 * Created by Sergiu on 6/19/2015.
 */

/// <reference path='../../../../typings/tsd.d.ts' />


module app.settings {
    'use strict';


    class SettingsController {
        constructor() {
        }
    }

    angular
        .module('app.settings')
        .controller('app.settings.SettingsController', SettingsController);
}

