/// <reference path='../../../../typings/tsd.d.ts' />
module app.chat {
    'use strict';

    interface IChatControllerScope {
        chats: app.chat.IChat[];

        remove(chat:app.chat.IChat): void;
    }

    class ChatController implements IChatControllerScope {
        chats:app.chat.IChat[];

        static $inject = ['app.chat.ChatService'];

        constructor(private chatsService:app.chat.IChatsService) {

            this.chats = chatsService.all();
        }

        remove(chat:app.chat.IChat):void {
            this.chatsService.remove(chat);
        }
    }

    angular
        .module('app.chat')
        .controller('app.chat.ChatController', ChatController);
}
