/// <reference path='../../../../typings/tsd.d.ts' />
/// <reference path='chats-service.ts' />
var app;
(function (app) {
    var chat;
    (function (chat) {
        'use strict';
        var ChatDetailController = (function () {
            function ChatDetailController(stateParams, chatsService) {
                this.chat = chatsService.get(+(stateParams['chatId']));
            }
            ChatDetailController.$inject = ['$stateParams', 'app.chat.ChatService'];
            return ChatDetailController;
        })();
        angular
            .module('app.chat')
            .controller('app.chat.ChatDetailController', ChatDetailController);
    })(chat = app.chat || (app.chat = {}));
})(app || (app = {}));
//# sourceMappingURL=chat-detail.js.map