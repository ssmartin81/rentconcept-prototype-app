/// <reference path='../../../../typings/tsd.d.ts' />
var app;
(function (app) {
    var chat;
    (function (chat_1) {
        'use strict';
        var ChatController = (function () {
            function ChatController(chatsService) {
                this.chatsService = chatsService;
                this.chats = chatsService.all();
            }
            ChatController.prototype.remove = function (chat) {
                this.chatsService.remove(chat);
            };
            ChatController.$inject = ['app.chat.ChatService'];
            return ChatController;
        })();
        angular
            .module('app.chat')
            .controller('app.chat.ChatController', ChatController);
    })(chat = app.chat || (app.chat = {}));
})(app || (app = {}));
//# sourceMappingURL=chats.js.map