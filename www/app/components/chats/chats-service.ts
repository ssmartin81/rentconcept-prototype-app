/// <reference path='../../../../typings/tsd.d.ts' />

module app.chat {
    'use strict';

    export interface IChat {
        id: number;
        name: string;
        lastText: string;
        face: string;
    }

    export interface IChatsService {
        all(): IChat[];
        remove(chat:IChat): void;
        get(chatId:number): IChat;
    }

    class ChatService implements IChatsService {
        private chats:IChat[];

        constructor() {
            this.chats = [];
            this.chats.push(<IChat>{
                id: 0,
                name: 'Ben Sparrow',
                lastText: 'You on your way?',
                face: 'https://pbs.twimg.com/profile_images/514549811765211136/9SgAuHeY.png'
            });
            this.chats.push(<IChat>{
                id: 1,
                name: 'Max Lynx',
                lastText: 'Hey, it\'s me',
                face: 'https://avatars3.githubusercontent.com/u/11214?v=3&s=460'
            });
            this.chats.push(<IChat>{
                id: 2,
                name: 'Adam Bradleyson',
                lastText: 'I should buy a boat',
                face: 'https://pbs.twimg.com/profile_images/479090794058379264/84TKj_qa.jpeg'
            });
            this.chats.push(<IChat>{
                id: 3,
                name: 'Perry Governor',
                lastText: 'Look at my mukluks!',
                face: 'https://pbs.twimg.com/profile_images/598205061232103424/3j5HUXMY.png'
            });
            this.chats.push(<IChat>{
                id: 4,
                name: 'Mike Harrington',
                lastText: 'This is wicked good ice cream.',
                face: 'https://pbs.twimg.com/profile_images/578237281384841216/R3ae1n61.png'
            });
        }

        all():IChat[] {
            return this.chats;
        }

        remove(chat:IChat):void {
            this.chats.splice(this.chats.indexOf(chat), 1);
        }

        get(chatId:number):IChat {
            for (var i:number = 0; i < this.chats.length; i++) {
                if (this.chats[i].id === chatId) {
                    return this.chats[i];
                }
            }
            return null;
        }
    }

    angular
        .module('app.chat')
        .service('app.chat.ChatService', ChatService);
}
