/// <reference path='../../../../typings/tsd.d.ts' />
/// <reference path='chats-service.ts' />

module app.chat {
	'use strict';

	interface IChatDetailControllerScope {
		chat: app.chat.IChat;
	}
	
    class ChatDetailController implements IChatDetailControllerScope{
        chat: app.chat.IChat;

        static $inject = ['$stateParams', 'app.chat.ChatService'];
        constructor(
            stateParams: angular.ui.IStateParamsService,
            chatsService: app.chat.IChatsService) {

            this.chat = chatsService.get(+(stateParams['chatId']));
        }
    }

	angular
        .module('app.chat')
        .controller('app.chat.ChatDetailController', ChatDetailController);
}
