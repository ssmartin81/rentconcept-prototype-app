/// <reference path='../../../../typings/tsd.d.ts' />
var app;
(function (app) {
    var home;
    (function (home) {
        'use strict';
        var HomeController = (function () {
            function HomeController() {
            }
            return HomeController;
        })();
        angular
            .module('app.home')
            .controller('app.home.HomeController', HomeController);
    })(home = app.home || (app.home = {}));
})(app || (app = {}));
//# sourceMappingURL=home.js.map