/// <reference path='../../../../typings/tsd.d.ts' />

module app.home {
    'use strict';

  
    class HomeController {
        constructor() {
        }
    }

    angular
        .module('app.home')
        .controller('app.home.HomeController', HomeController);
}
