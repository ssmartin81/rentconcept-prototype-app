/// <reference path='../../../../typings/tsd.d.ts' />
var app;
(function (app) {
    var dash;
    (function (dash) {
        'use strict';
        var DashController = (function () {
            function DashController() {
            }
            return DashController;
        })();
        angular
            .module('app.dash')
            .controller('app.dash.DashController', DashController);
    })(dash = app.dash || (app.dash = {}));
})(app || (app = {}));
//# sourceMappingURL=dash.js.map