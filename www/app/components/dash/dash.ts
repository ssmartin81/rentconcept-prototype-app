/// <reference path='../../../../typings/tsd.d.ts' />

module app.dash {
	'use strict';

	interface IDashControllerScope {
	}
	
    class DashController implements IDashControllerScope {
        constructor() {
        }
    }

	angular
		.module('app.dash')
        .controller('app.dash.DashController', DashController);
}
