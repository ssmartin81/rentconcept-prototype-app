/**
 * Created by Sergiu on 6/19/2015.
 */

/// <reference path='../../../../typings/tsd.d.ts' />

module app.booking {
    'use strict';


    class MyBookingsController {
        constructor() {
        }
    }

    angular
        .module('app.booking')
        .controller('app.booking.MyBookingsController', MyBookingsController);
}


