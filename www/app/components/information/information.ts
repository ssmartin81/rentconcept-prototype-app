/**
 * Created by Sergiu on 6/19/2015.
 */

/// <reference path='../../../../typings/tsd.d.ts' />

module app.information {
    'use strict';


    class InformationController {
        constructor() {
        }
    }

    angular
        .module('app.home')
        .controller('app.information.InformationController', InformationController);
}

