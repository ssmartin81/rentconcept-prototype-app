/// <reference path='../../typings/tsd.d.ts' />
(function () {
    'use strict';
    angular.module('app', [
        'ionic',
        'ui.router',
        'pascalprecht.translate',
        'app.home',
        'app.dash',
        'app.account',
        'app.chat'
    ])
        .config(config)
        .run(main);
    angular.module('app.home', []);
    angular.module('app.dash', []);
    angular.module('app.account', []);
    angular.module('app.chat', []);
    config.$inject = ['$stateProvider', '$urlRouterProvider', '$translateProvider', '$ionicConfigProvider'];
    function config(stateProvider, urlProvider, translateProvider, ionicConfigProvider) {
        configStates(stateProvider);
        urlProvider.otherwise('/tab/dash');
        translateProvider.useStaticFilesLoader({
            prefix: '/i18n/',
            suffix: '.json'
        });
        translateProvider.preferredLanguage("en");
        translateProvider.fallbackLanguage("en");
        ionicConfigProvider.tabs.position('bottom');
    }
    function configStates(stateProvider) {
        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js
        var tabState = {
            url: "/tab",
            abstract: true,
            templateUrl: "app/tabs.html"
        };
        stateProvider.state('tab', tabState);
        var homeState = {
            name: 'tab.home',
            url: '/home',
            views: {
                'tab-home': {
                    templateUrl: 'app/components/home/home.html',
                    controller: 'app.home.HomeController as ctrl'
                }
            }
        };
        stateProvider.state(homeState);
        var dashState = {
            name: 'tab.dash',
            url: '/dash',
            views: {
                'tab-dash': {
                    templateUrl: 'app/components/dash/tab-dash.html',
                }
            }
        };
        stateProvider.state(dashState);
        var chartsState = {
            name: 'tab.chats',
            url: '/chats',
            views: {
                'tab-chats': {
                    templateUrl: 'app/components/chats/tab-chats.html'
                }
            }
        };
        stateProvider.state(chartsState);
        var detailState = {
            name: 'tab.chat-detail',
            url: '/chats/:chatId',
            views: {
                'tab-chats': {
                    templateUrl: 'app/components/chats/chat-detail.html',
                    controller: 'app.chat.ChatDetailController as ctrl'
                }
            }
        };
        stateProvider.state(detailState);
        var accountState = {
            name: 'tab.account',
            url: '/account',
            views: {
                'tab-account': {
                    templateUrl: 'app/components/account/tab-account.html',
                    controller: 'app.account.AccountController as ctrl'
                }
            }
        };
        stateProvider.state(accountState);
    }
    main.$inject = ['$ionicPlatform', '$translate'];
    function main(ionicPlatform, translateService) {
        ionicPlatform.ready(function () {
            ready(ionicPlatform, translateService);
        });
    }
    function ready(ionicPlatform, translateService) {
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            window.cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            window.StatusBar.styleLightContent();
        }
        var userLang = navigator.language || navigator.userLanguage;
        translateService.use(userLang.substr(0, 2)).then(function (data) {
            console.log("SUCCESS -> " + data);
        }, function (error) {
            console.log("ERROR -> " + error);
        });
    }
})();
//# sourceMappingURL=app.js.map